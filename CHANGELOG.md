# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.4.0] - 2023-09-29
### Added
- (#23) Updated `MAX_FEATURE_VALS` to include previously missing wikis.

### Changed
- (#23) BREAKING: `QualityFeatures` now raises an `UnsupportedWikiException` for wikis that we don't have quality feature values for.
- (#23) BREAKING: `RevertRiskModel` has now been bumped to version 2.0. Any serialized models with an older version number will raise a warning when loaded.

## [0.3.0] - 2023-07-27
### Added
- (#16) Added new model for predicting reverts on Wikidata

### Changed
- (#16) BREAKING: `REVISION_SCHEMA` now requires `user_name`.
- (#17) Replaced `EditTypes` from `mwedittypes` with the new `SimpleEditTypes` in Revertrisk Multilingual for improved stability and performance.

## [0.2.0] - 2022-12-14
### Added
- (#7) Added content oriented Knowledge Integrity model for predicting reverts.

### Changed
- (#7) BREAKING:  `REVISION_SCHEMA` now requires `page_title`, `rev_tags` and `parent_tags`.

## [0.1.1] - 2022-12-14
### Added
- (#5) Package is now `PEP-561` compliant

## [0.1.0] - 2022-10-25
First release.
