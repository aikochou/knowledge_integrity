import datetime

from dataclasses import replace
from typing import Any, Dict, Sequence

import pytest

from knowledge_integrity.featureset import (
    ContentFeatures,
    FeatureSource,
    PageFeatures,
    QualityFeatures,
    UserFeatures,
    get_features,
)
from knowledge_integrity.revision import CurrentRevision, Page, ParentRevision, User


@pytest.fixture
def example_text() -> str:
    return """This is a lead.
        == Section I ==
        Section I body. {{and a|template}}
        === Section I.A ===
        Section I.A [[body]].
        === Section I.B ===
        ==== Section I.B.1 ====
        Section I.B.1 body.

        &bull;Some content with [[page|link]].

        == Section II ==
        Section II body.
        [[File:image.jpg|thumb|right|150px|An image]]

        == Section III ==
        === Section III.A ===
        Text.<ref name="foo">a ref</ref>
        ===== Section III.A.1.a =====
        More text.<ref name="foo" />
        ==== Section III.A.2 ====
        Even more text.

        [[Category:bar]]
        [[Category:baz]]
    """


@pytest.fixture
def example_revision(example_text: str) -> CurrentRevision:
    user = User(
        id=1,
        name="Athena",
        editcount=25,
        groups=["*", "user", "autoconfirmed"],
        registration_timestamp=datetime.datetime(
            2020, 1, 1, 16, 30, 0, tzinfo=datetime.timezone.utc
        ),
    )
    page = Page(
        id=45,
        title="this is a title",
        first_edit_timestamp=datetime.datetime(
            2007, 1, 1, 18, 30, 0, tzinfo=datetime.timezone.utc
        ),
    )
    parent = ParentRevision(
        id=1020,
        bytes=2048,
        comment="this is a revision",
        lang="en",
        timestamp=datetime.datetime(2021, 1, 1, 5, 0, 0, tzinfo=datetime.timezone.utc),
        text=example_text,
        tags=[],
    )
    revision = CurrentRevision(
        id=1035,
        bytes=2052,
        comment="added category qux",
        lang="en",
        timestamp=datetime.datetime(2021, 1, 1, 8, 30, 0, tzinfo=datetime.timezone.utc),
        text=example_text + "\n[[Category:baz]]\n",
        page=page,
        parent=parent,
        user=user,
        tags=["foo"],
    )
    return revision


def test_user_features(example_revision: CurrentRevision) -> None:
    features = UserFeatures(example_revision)
    expected = dict(
        user_age=365,
        user_is_anonymous=0,
        user_is_bot=0,
        user_revision_count=25,
        user_groups=["*", "user", "autoconfirmed"],
    )
    assert features.to_dict() == expected


def test_anonymous_user_features(example_revision: CurrentRevision) -> None:
    user = User(id=0, name=None, editcount=0, groups=[], registration_timestamp=None)
    revision = replace(example_revision, user=user)
    features = UserFeatures(revision)

    expected = dict(
        user_age=0,
        user_is_anonymous=1,
        user_is_bot=0,
        user_revision_count=0,
        user_groups=[],
    )

    assert features.to_dict() == expected


@pytest.mark.parametrize(
    ("groups", "is_bot"), ((["bot"], 1), (["bot", "other"], 1), (["other"], 0))
)
def test_group_bot_user_features(
    example_revision: CurrentRevision, groups: Sequence[int], is_bot: int
) -> None:
    user = replace(example_revision.user, groups=groups)
    revision = replace(example_revision, user=user)
    features = UserFeatures(revision)
    assert features.user_is_bot == is_bot


@pytest.mark.parametrize(
    ("username", "is_bot"),
    (
        ("beeBot", 1),
        ("beeBot1", 1),
        ("beeBot1Suffix", 1),
        ("beebot", 1),
        ("beebot2", 1),
        ("beebot2suffix", 1),
        ("nobothere", 0),
        ("noBothereEither", 0),
    ),
)
def test_name_bot_user_features(
    example_revision: CurrentRevision, username: str, is_bot: int
) -> None:
    user = replace(example_revision.user, name=username, groups=[])
    revision = replace(example_revision, user=user)
    features = UserFeatures(revision)
    assert features.user_is_bot == is_bot


def test_page_features(example_revision: CurrentRevision) -> None:
    features = PageFeatures(example_revision)
    expected = dict(
        page_age=5113,
        page_seconds_since_previous_revision=12600,
        page_title="this is a title",
    )
    assert features.to_dict() == expected


def test_content_features(example_revision: CurrentRevision) -> None:
    features = ContentFeatures(example_revision)
    expected = dict(
        category_count=3,
        comment="added category qux",
        has_comment=True,
        heading_count=6,
        media_count=1,
        reference_count=2,
        revision_text_bytes=2052,
        tags=["foo"],
        text_length=686,
        wikilink_count=2,
        wikitext=example_revision.text,
        wiki_db="enwiki",
    )
    assert features.to_dict() == expected


def test_quality_features(example_revision: CurrentRevision) -> None:
    features = QualityFeatures(example_revision)
    expected = dict(
        norm_categories=0.214,
        norm_headings=1,
        norm_media=0.333,
        norm_references=0.259,
        norm_length=0.158,
        norm_wikilinks=0.466,
        quality_range=3,
        quality_score=0.339,
    )
    assert features.to_dict() == pytest.approx(expected, abs=0.001)


def test_get_features(example_revision: CurrentRevision) -> None:
    feature_sources = (
        FeatureSource(ContentFeatures(example_revision)),
        FeatureSource(ContentFeatures(example_revision.parent), prefix="parent_"),
    )
    features = get_features(
        feature_sources,
        ("revision_text_bytes", "revision_text_bytes_diff"),
        transformer,
    )
    assert features["revision_text_bytes"] == 2052
    assert features["revision_text_bytes_diff"] == 4


def transformer(f: Dict[str, Any]) -> Dict[str, Any]:
    return dict(
        revision_text_bytes_diff=(
            f["revision_text_bytes"] - f["parent_revision_text_bytes"]
        )
    )
