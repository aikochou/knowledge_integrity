import asyncio

from typing import Any, Generator

import pytest_asyncio


@pytest_asyncio.fixture(scope="session")
def event_loop(request: Any) -> Generator[asyncio.AbstractEventLoop, None, None]:
    loop = asyncio.get_event_loop()
    yield loop
    loop.close()
