import pytest

from knowledge_integrity.models.revertrisk_multilingual.model import (
    diff_text,
    get_edit_actions,
    one_hot_encode,
)


@pytest.fixture
def lang() -> str:
    return "en"


@pytest.fixture
def prev_wikitext() -> str:
    return """This is a lead.
== Section I ==
Sec I body.
=== Section I.A ===
Section I.A [[body]].
"""


@pytest.fixture
def curr_wikitext() -> str:
    return """This is a lead.
== Section I ==
Sec I body. {{and a|template}}
== Section II ==
=== Section II.A ===
Section II.A body.
"""


def test_get_edit_actions(prev_wikitext: str, curr_wikitext: str, lang: str) -> None:
    edit_actions = get_edit_actions(prev_wikitext, curr_wikitext, lang)

    for action_type, count in [
        ("remove_Wikilink", 1),
        ("insert_Template", 1),
        ("insert_Heading", 1),
        ("change_Heading", 1),
        ("change_Word", 1),
        ("change_Sentence", 1),
        ("change_Paragraph", 1),
        ("change_Section", 2),
        ("insert_Section", 1),
        ("insert_Whitespace", 2),
    ]:
        assert edit_actions[action_type] == count


def test_diff_text(prev_wikitext: str, curr_wikitext: str, lang: str) -> None:
    text_diff = diff_text(prev_wikitext, curr_wikitext, lang)

    for attr, expected in [
        (text_diff.added, []),
        (text_diff.deleted, []),
        (sorted(text_diff.changed), sorted([("Section I", "Section II")])),
    ]:
        assert attr == expected


def test_one_hot_encode() -> None:
    sample = ["b"]
    labels = ["a", "b", "c"]
    prefix = ""

    encoded_sample = one_hot_encode(sample, labels, prefix=prefix)

    assert encoded_sample == dict(a=0, b=1, c=0)
