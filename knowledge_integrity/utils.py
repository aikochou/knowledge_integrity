from datetime import datetime


def parse_timestamp(timestamp: str) -> datetime:
    # MW API returns timestamps in Zulu format which
    # python datetime does not recognize.
    formatted_timestamp = timestamp.replace("Z", "+00:00")
    return datetime.fromisoformat(formatted_timestamp)
