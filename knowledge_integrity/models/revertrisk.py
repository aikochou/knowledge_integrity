import logging
import math
import pathlib
import sys

from dataclasses import dataclass
from typing import Any, Dict, List, Sequence

import joblib  # type: ignore
import pandas as pd  # type: ignore
import xgboost as xgb  # type: ignore

from knowledge_integrity.featureset import (
    ContentFeatures,
    FeatureSource,
    PageFeatures,
    QualityFeatures,
    UserFeatures,
    get_features,
)
from knowledge_integrity.revision import CurrentRevision


MODEL_VERSION: int = 2


@dataclass
class RevertRiskModel:
    model_version: int
    classifier: xgb.Booster
    supported_wikis: List[str]


@dataclass
class ClassifyResult:
    prediction: bool
    probability: float


def extract_features(
    revision: CurrentRevision, features: Sequence[str]
) -> Dict[str, Any]:
    # groups of features required for the current revision
    user_features = UserFeatures(revision)
    page_features = PageFeatures(revision)
    content_features = ContentFeatures(revision)
    quality_features = QualityFeatures(revision)

    # groups of features required for the parent revision
    parent_content_features = ContentFeatures(revision.parent)
    parent_quality_features = QualityFeatures(revision.parent)

    feature_sources = (
        FeatureSource(source=user_features),
        FeatureSource(source=page_features),
        FeatureSource(source=content_features),
        FeatureSource(source=quality_features),
        FeatureSource(source=parent_content_features, prefix="parent_"),
        FeatureSource(source=parent_quality_features, prefix="parent_"),
    )
    return get_features(
        feature_sources, features, _transformed_revision_integrity_features
    )


def _transformed_revision_integrity_features(
    features: Dict[str, Any]
) -> Dict[str, Any]:
    quality_change = features["quality_score"] - features["parent_quality_score"]
    byte_diff = features["revision_text_bytes"] - features["parent_revision_text_bytes"]
    user_groups = len(features["user_groups"])

    return dict(
        quality_change=quality_change,
        quality_change_range=math.floor(10 * quality_change),
        revision_text_bytes_diff=byte_diff,
        user_groups=user_groups,
    )


def load_model(model_path: pathlib.Path) -> RevertRiskModel:
    # ensure that joblib can find the model in global symbols when unpickling
    sys.modules["__main__"].RevertRiskModel = RevertRiskModel  # type: ignore

    with open(model_path, "rb") as f:
        model: RevertRiskModel = joblib.load(f)

    if model.model_version != MODEL_VERSION:
        logging.warn(
            "Serialized model version %d does not match current model version %d",
            model.model_version,
            MODEL_VERSION,
        )
    return model


def classify(model: RevertRiskModel, revision: CurrentRevision) -> ClassifyResult:
    feature_names = model.classifier.feature_names
    if feature_names is None:
        raise ValueError("feature_names for serialized classifier cannot be None.")

    features = extract_features(revision, feature_names)
    X = xgb.DMatrix(pd.DataFrame([features]))
    [prob_yes] = model.classifier.predict(X, validate_features=True)

    return ClassifyResult(
        # convert numpy values to make them serializable
        prediction=bool(prob_yes > 0.5),
        probability=float(prob_yes),
    )
