import logging
import pathlib
import sys

from dataclasses import dataclass
from typing import Any, Dict, Optional, Sequence

import catboost as catb
import joblib
import sklearn.preprocessing as skp
import transformers

from typing_extensions import Literal, TypeAlias

from knowledge_integrity.featureset import (
    ContentFeatures,
    FeatureSource,
    PageFeatures,
    Transformer,
    UserFeatures,
    get_features,
)
from knowledge_integrity.models.revertrisk_wikidata.edit import (
    ClaimEdit,
    DescriptionEdit,
    EditKind,
    parse_edit,
)
from knowledge_integrity.revision import CurrentRevision


MODEL_VERSION: int = 1


@dataclass
class ClaimModel:
    metadata_classifier: catb.CatBoostClassifier
    label_binarizer: skp.MultiLabelBinarizer


@dataclass
class DescriptionModel:
    metadata_classifier: catb.CatBoostClassifier
    text_classifier: transformers.Pipeline
    label_binarizer: skp.MultiLabelBinarizer


@dataclass
class RevertRiskWikidataModel:
    claim_model: ClaimModel
    desc_model: DescriptionModel
    model_version: int


@dataclass
class ClassifyResult:
    prediction: bool
    probability: float


Features: TypeAlias = Dict[str, Any]


def get_revision_features(
    revision: CurrentRevision,
    feature_names: Sequence[str],
    transformer: Optional[Transformer] = None,
) -> Features:
    user_features = UserFeatures(revision)
    page_features = PageFeatures(revision)
    content_features = ContentFeatures(revision)

    feature_sources = (
        FeatureSource(source=user_features),
        FeatureSource(source=page_features),
        FeatureSource(source=content_features),
    )
    return get_features(feature_sources, feature_names, transformer)


def multi_hot_encode(
    label_binarizer: skp.MultiLabelBinarizer, labels: Sequence[str]
) -> Dict[str, Literal[0, 1]]:
    all_labels = label_binarizer.classes_
    multi_hot_labels = label_binarizer.transform([labels])
    return dict(zip(all_labels, *multi_hot_labels))


class ClaimPipeline:
    """Pipeline for classifying Wikidata `ClaimEdit` revisions
    using `ClaimModel`.
    """

    def __init__(self, model: ClaimModel) -> None:
        self.model = model
        self.feature_names = self.model.metadata_classifier.feature_names_

    def _transform_revision_features(self, features: Features) -> Features:
        tag_labels = [f"revision_tags-{tag}" for tag in features["tags"]]
        group_labels = [f"event_user_groups-{grp}" for grp in features["user_groups"]]
        labels = tag_labels + group_labels
        categorical_features = multi_hot_encode(self.model.label_binarizer, labels)
        return categorical_features

    def __call__(self, revision: CurrentRevision, edit: ClaimEdit) -> ClassifyResult:
        revision_feature_names = [ft for ft in self.feature_names if ft != "prop"]
        features = get_revision_features(
            revision,
            revision_feature_names,
            self._transform_revision_features,
        )
        features["prop"] = edit.prop

        X = [features[name] for name in self.feature_names]
        [_, prob_yes] = self.model.metadata_classifier.predict_proba(X)

        return ClassifyResult(
            prediction=bool(prob_yes > 0.5),
            probability=float(prob_yes),
        )


class DescriptionPipeline:
    """Pipeline for classifying Wikidata `DescriptionEdit`
    revisions using `DescriptionModel`.
    """

    def __init__(self, model: DescriptionModel) -> None:
        self.model = model
        self.feature_names = self.model.metadata_classifier.feature_names_

    def _transform_revision_features(self, features: Features) -> Features:
        tag_labels = [f"revision_tags-{tag}" for tag in features["tags"]]
        group_labels = [f"event_user_groups-{grp}" for grp in features["user_groups"]]
        labels = tag_labels + group_labels
        categorical_features = multi_hot_encode(self.model.label_binarizer, labels)
        return categorical_features

    def __call__(
        self,
        revision: CurrentRevision,
        edit: DescriptionEdit,
    ) -> ClassifyResult:
        revision_feature_names = [
            ft for ft in self.feature_names if ft != "prob_desc_is_reverted"
        ]
        features = get_revision_features(
            revision,
            revision_feature_names,
            self._transform_revision_features,
        )
        desc_pred = self.model.text_classifier(edit.description, top_k=2)
        [desc_prob] = [p["score"] for p in desc_pred if p["label"] == "reverted"]
        features["prob_desc_is_reverted"] = desc_prob

        X = [features[name] for name in self.feature_names]
        [_, prob_yes] = self.model.metadata_classifier.predict_proba(X)

        return ClassifyResult(
            prediction=bool(prob_yes > 0.5),
            probability=float(prob_yes),
        )


def load_model(model_path: pathlib.Path) -> RevertRiskWikidataModel:
    # ensure that joblib can find the model in global symbols when unpickling
    setattr(sys.modules["__main__"], "RevertRiskWikidataModel", RevertRiskWikidataModel)
    setattr(sys.modules["__main__"], "ClaimModel", ClaimModel)
    setattr(sys.modules["__main__"], "DescriptionModel", DescriptionModel)

    with open(model_path, "rb") as f:
        model: RevertRiskWikidataModel = joblib.load(f)

    if model.model_version != MODEL_VERSION:
        logging.warn(
            "Serialized model version %d does not match current model version %d",
            model.model_version,
            MODEL_VERSION,
        )
    return model


def classify(
    model: RevertRiskWikidataModel, revision: CurrentRevision
) -> Optional[ClassifyResult]:
    """Classifies a wikidata revision based on the type of edit
    indicated by the revision summary. Returns `None` if the edit
    does not belong to a supported type.
    """
    edit = parse_edit(revision.comment)

    if edit is None:
        return None

    if edit.kind is EditKind.CLAIM:
        claim_pipeline = ClaimPipeline(model.claim_model)
        return claim_pipeline(revision, edit)

    if edit.kind is EditKind.DESCRIPTION:
        description_pipeline = DescriptionPipeline(model.desc_model)
        return description_pipeline(revision, edit)
