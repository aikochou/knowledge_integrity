"""Revertrisk model for Wikidata."""

from knowledge_integrity.models.revertrisk_wikidata.model import (
    ClassifyResult,
    RevertRiskWikidataModel,
    classify,
    load_model,
)


__all__ = ["ClassifyResult", "RevertRiskWikidataModel", "classify", "load_model"]
