import itertools
import logging
import pathlib
import re
import sys

from dataclasses import dataclass
from typing import Any, Dict, List, Sequence, Set, Tuple

import catboost as catb  # type: ignore
import joblib  # type: ignore
import mwedittypes  # type: ignore
import transformers  # type: ignore

from fuzzywuzzy import fuzz  # type: ignore
from typing_extensions import TypeAlias

from knowledge_integrity.featureset import (
    ContentFeatures,
    FeatureSource,
    PageFeatures,
    QualityFeatures,
    UserFeatures,
    get_features,
)
from knowledge_integrity.models.revertrisk_multilingual.bert import (
    classify_changes,
    classify_inserts,
    classify_removes,
    classify_title,
)
from knowledge_integrity.revision import CurrentRevision


MODEL_VERSION: int = 4

ALLOWED_REVISION_TAGS = [
    "mobile edit",
    "mobile web edit",
    "visualeditor",
    "wikieditor",
    "mobile app edit",
    "android app edit",
    "ios app edit",
]
ALLOWED_USER_GROUPS = [
    "editor",
    "trusted",
    "ipblock-exempt",
    "interface-admin",
    "sysop",
    "autoreview",
    "bureaucrat",
    "autopatrolled",
    "patroller",
    "rollbacker",
    "reviewer",
    "extendedconfirmed",
    "templateeditor",
    "uploader",
    "autoreviewer",
    "suppressredirect",
]
ALLOWED_CONTENT_TYPES = [
    "Argument",
    "Category",
    "Comment",
    "ExternalLink",
    "Gallery",
    "HTMLEntity",
    "Heading",
    "List",
    "Media",
    "Paragraph",
    "Punctuation",
    "Reference",
    "Section",
    "Sentence",
    "Table",
    "Table Element",
    "Template",
    "Text",
    "Text Formatting",
    "Whitespace",
    "Wikilink",
    "Word",
]
ALLOWED_ACTION_TYPES = ["change", "insert", "remove"]

CLASSIFIER_INTERMEDIATE_FEATURES = [
    "page_title",
    "comment",
    "wiki_db",
    "revision_text_bytes_diff",
    "is_editor",
    "is_trusted",
    "is_ipblock-exempt",
    "is_interface-admin",
    "is_sysop",
    "is_autoreview",
    "is_bureaucrat",
    "is_autopatrolled",
    "is_patroller",
    "is_rollbacker",
    "is_reviewer",
    "is_extendedconfirmed",
    "is_templateeditor",
    "is_uploader",
    "is_autoreviewer",
    "is_suppressredirect",
    "user_is_anonymous",
    "is_mobile_edit",
    "is_mobile_web_edit",
    "is_visualeditor",
    "is_wikieditor",
    "is_mobile_app_edit",
    "is_android_app_edit",
    "is_ios_app_edit",
    "texts_removed",
    "texts_insert",
    "texts_change",
    "change_Argument",
    "insert_Argument",
    "remove_Argument",
    "change_Category",
    "insert_Category",
    "remove_Category",
    "change_Comment",
    "insert_Comment",
    "remove_Comment",
    "change_ExternalLink",
    "insert_ExternalLink",
    "remove_ExternalLink",
    "change_Gallery",
    "insert_Gallery",
    "remove_Gallery",
    "change_HTMLEntity",
    "insert_HTMLEntity",
    "remove_HTMLEntity",
    "change_Heading",
    "insert_Heading",
    "remove_Heading",
    "change_List",
    "insert_List",
    "remove_List",
    "change_Media",
    "insert_Media",
    "remove_Media",
    "change_Paragraph",
    "insert_Paragraph",
    "remove_Paragraph",
    "change_Punctuation",
    "insert_Punctuation",
    "remove_Punctuation",
    "change_Reference",
    "insert_Reference",
    "remove_Reference",
    "change_Section",
    "insert_Section",
    "remove_Section",
    "change_Sentence",
    "insert_Sentence",
    "remove_Sentence",
    "change_Table",
    "insert_Table",
    "remove_Table",
    "change_Table Element",
    "insert_Table Element",
    "remove_Table Element",
    "change_Template",
    "insert_Template",
    "remove_Template",
    "change_Text",
    "insert_Text",
    "remove_Text",
    "change_Text Formatting",
    "insert_Text Formatting",
    "remove_Text Formatting",
    "change_Whitespace",
    "insert_Whitespace",
    "remove_Whitespace",
    "change_Wikilink",
    "insert_Wikilink",
    "remove_Wikilink",
    "change_Word",
    "insert_Word",
    "remove_Word",
]


@dataclass
class MultilingualRevertRiskModel:
    model_version: int
    classifier: catb.CatBoostClassifier
    change_model: transformers.Pipeline
    insert_model: transformers.Pipeline
    title_model: transformers.Pipeline
    remove_model: transformers.Pipeline
    supported_wikis: List[str]


@dataclass
class ClassifyResult:
    prediction: bool
    probability: float


@dataclass(frozen=True)
class TextDiff:
    added: List[str]
    deleted: List[str]
    changed: List[Tuple[str, str]]


EditActions: TypeAlias = Dict[str, int]


def sentence_tokenize(text: str) -> List[str]:
    """
    Basic method used to split text into sentences
    """
    return list(map(str.strip, re.split(r"[.!?](?!$)", text)))


def flatten_list(list_of_items: List[List[Any]]) -> List[Any]:
    """
    Method to flatten the list
    """
    return [item for sublist in list_of_items for item in sublist]


def diff_text(prev_wikitext: str, curr_wikitext: str, lang: str) -> TextDiff:
    """Returns lines added, deleted and changed in `curr_wikitext` as `TextDiff`
    by comparing it to `prev_wikitext`.
    """
    prev_text = mwedittypes.utils.wikitext_to_plaintext(prev_wikitext, lang=lang)
    curr_text = mwedittypes.utils.wikitext_to_plaintext(curr_wikitext, lang=lang)
    text_diff = mwedittypes.tokenizer.parse_change_text(
        prev_text,
        curr_text,
        lang=lang,
        summarize=False,
    )
    sentence_diff = text_diff.get("Sentence", {})

    lines_added = set(key for key, val in sentence_diff.items() if val > 0)
    lines_deleted = set(key for key, val in sentence_diff.items() if val < 0)
    lines_changed = []

    seen: Set[str] = set()
    for deleted_line, added_line in itertools.product(lines_deleted, lines_added):
        if deleted_line not in seen and added_line not in seen:
            similarity = fuzz.ratio(deleted_line, added_line)
            if similarity > 60:
                lines_changed.append((deleted_line, added_line))
                seen.update((deleted_line, added_line))

    return TextDiff(
        added=list(lines_added - seen),
        deleted=list(lines_deleted - seen),
        changed=lines_changed,
    )


def get_edit_actions(prev_wikitext: str, curr_wikitext: str, lang: str) -> EditActions:
    """Compares `prev_wikitext` and `curr_wikitext` and returns
    edit actions that converted the former into the latter.
    """
    et = mwedittypes.SimpleEditTypes(prev_wikitext, curr_wikitext, lang)
    actions = et.get_diff()
    filtered_actions = {
        f"{at}_{ct}": actions.get(ct, {}).get(at, 0)
        for ct in ALLOWED_CONTENT_TYPES
        for at in ALLOWED_ACTION_TYPES
    }
    return filtered_actions


def one_hot_encode(
    sample: List[str], labels: List[str], prefix: str = "is_"
) -> Dict[str, int]:
    """Returns a dictionary of one hot encoded sample labels."""
    # convert to set so that we can do a membership test in O(1)
    # for each label in sample
    sample_set = set(sample)
    return {
        prefix + label.replace(" ", "_"): 1 if label in sample_set else 0
        for label in labels
    }


def _transformed_revertrisk_features(features: Dict[str, Any]) -> Dict[str, Any]:
    byte_diff = features["revision_text_bytes"] - features["parent_revision_text_bytes"]
    one_hot_tags = one_hot_encode(features["tags"], ALLOWED_REVISION_TAGS)
    one_hot_user_tags = one_hot_encode(features["user_groups"], ALLOWED_USER_GROUPS)
    edit_actions = get_edit_actions(
        features["parent_wikitext"],
        features["wikitext"],
        features["wiki_db"].replace("wiki", ""),
    )
    text_diff = diff_text(
        features["parent_wikitext"],
        features["wikitext"],
        features["wiki_db"].replace("wiki", ""),
    )

    return dict(
        revision_text_bytes_diff=byte_diff,
        texts_removed=text_diff.deleted,
        texts_insert=text_diff.added,
        texts_change=text_diff.changed,
        **edit_actions,
        **one_hot_tags,
        **one_hot_user_tags,
    )


def extract_features(
    revision: CurrentRevision, features: Sequence[str]
) -> Dict[str, Any]:
    # groups of features required for the current revision
    user_features = UserFeatures(revision)
    page_features = PageFeatures(revision)
    content_features = ContentFeatures(revision)
    quality_features = QualityFeatures(revision)

    # groups of features required for the parent revision
    parent_content_features = ContentFeatures(revision.parent)
    parent_quality_features = QualityFeatures(revision.parent)

    feature_sources = (
        FeatureSource(source=user_features),
        FeatureSource(source=page_features),
        FeatureSource(source=content_features),
        FeatureSource(source=quality_features),
        FeatureSource(source=parent_content_features, prefix="parent_"),
        FeatureSource(source=parent_quality_features, prefix="parent_"),
    )
    return get_features(feature_sources, features, _transformed_revertrisk_features)


def load_model(model_path: pathlib.Path) -> MultilingualRevertRiskModel:
    # ensure that joblib can find the model in global symbols when unpickling
    sys.modules["__main__"].MultilingualRevertRiskModel = MultilingualRevertRiskModel  # type: ignore  # noqa: E501

    with open(model_path, "rb") as f:
        model: MultilingualRevertRiskModel = joblib.load(f)

    if model.model_version != MODEL_VERSION:
        logging.warn(
            "Serialized model version %d does not match current model version %d",
            model.model_version,
            MODEL_VERSION,
        )
    return model


def classify(
    model: MultilingualRevertRiskModel, revision: CurrentRevision
) -> ClassifyResult:
    feature_names = model.classifier.feature_names_
    if feature_names is None:
        raise ValueError("feature_names for serialized classifier cannot be None.")

    intermediate_features = extract_features(revision, CLASSIFIER_INTERMEDIATE_FEATURES)

    # add Bert classification of text elements as features
    features = {
        **classify_title(model.title_model, intermediate_features["page_title"]),
        **classify_removes(model.remove_model, intermediate_features["texts_removed"]),
        **classify_inserts(model.insert_model, intermediate_features["texts_insert"]),
        **classify_changes(model.change_model, intermediate_features["texts_change"]),
        **intermediate_features,
    }

    X = [features[name] for name in feature_names]
    [_, prob_yes] = model.classifier.predict_proba(X)
    return ClassifyResult(
        # convert numpy values to make them serializable
        prediction=bool(prob_yes > 0.5),
        probability=float(prob_yes),
    )
