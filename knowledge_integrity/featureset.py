import math
import re

from abc import ABC, abstractmethod
from dataclasses import dataclass
from typing import Any, Callable, Dict, List, Optional, Sequence

from knowledge_integrity import constants
from knowledge_integrity.revision import CurrentRevision, Revision


class UnsupportedWikiException(Exception):
    def __init__(self, wiki: str, feature_group: str) -> None:
        super().__init__(
            f"Wiki {wiki} is not supported by feature group {feature_group}"
        )


class Features(ABC):
    """Abstract base class for features whose concrete
    implementations represent any logical group of features.
    """

    @abstractmethod
    def to_dict(self) -> Dict[str, Any]:
        """Returns all features implemented by the class as
        a dictionary. This method assumes that all public properties
        exposed by the class represent features.
        """
        return {
            key: getattr(self, key)
            for key, value in vars(self.__class__).items()
            if isinstance(value, property)
        }


class UserFeatures(Features):
    """Features related to the user who authored the
    current revision.
    """

    def __init__(
        self,
        revision: CurrentRevision,
    ) -> None:
        self._revision = revision

    @property
    def user_groups(self) -> List[str]:
        return self._revision.user.groups

    @property
    def user_revision_count(self) -> int:
        return self._revision.user.editcount

    @property
    def user_is_anonymous(self) -> int:
        return self._revision.user.is_anonymous

    @property
    def user_is_bot(self) -> int:
        if "bot" in self._revision.user.groups:
            return 1

        username = self._revision.user.name
        if username is not None:
            bot_name_match = re.fullmatch(r".*bot([^a-z].*)?", username, re.I)
            if bot_name_match is not None:
                return 1

        return 0

    @property
    def user_age(self) -> int:
        if self._revision.user.registration_timestamp is None:
            return 0

        delta = self._revision.timestamp - self._revision.user.registration_timestamp
        return delta.days

    def to_dict(self) -> Dict[str, Any]:
        return super().to_dict()


class PageFeatures(Features):
    """Features related to the page associated with
    the current revision.
    """

    def __init__(
        self,
        revision: CurrentRevision,
    ) -> None:
        self._revision = revision

    @property
    def page_seconds_since_previous_revision(self) -> float:
        delta = self._revision.timestamp - self._revision.parent.timestamp
        return delta.total_seconds()

    @property
    def page_age(self) -> int:
        delta = self._revision.timestamp - self._revision.page.first_edit_timestamp
        return delta.days

    @property
    def page_title(self) -> str:
        return self._revision.page.title

    def to_dict(self) -> Dict[str, Any]:
        return super().to_dict()


class ContentFeatures(Features):
    """Features related to the content of a revision
    which includes the comment.
    """

    def __init__(
        self,
        revision: Revision,
    ) -> None:
        self._revision = revision

    @property
    def comment(self) -> str:
        return self._revision.comment

    @property
    def revision_text_bytes(self) -> int:
        return self._revision.bytes

    @property
    def has_comment(self) -> bool:
        return self._revision.comment != ""

    @property
    def text_length(self) -> int:
        return len(self._revision.text)

    @property
    def category_count(self) -> int:
        return len(self._revision.categories)

    @property
    def heading_count(self) -> int:
        return len(self._revision.headings)

    @property
    def reference_count(self) -> int:
        return len(self._revision.references)

    @property
    def media_count(self) -> int:
        return len(self._revision.media)

    @property
    def wikilink_count(self) -> int:
        return len(self._revision.wikilinks)

    @property
    def tags(self) -> List[str]:
        return self._revision.tags

    @property
    def wiki_db(self) -> str:
        return self._revision.lang + "wiki"

    @property
    def wikitext(self) -> str:
        return self._revision.text

    def to_dict(self) -> Dict[str, Any]:
        return super().to_dict()


class QualityFeatures(Features):
    """Features associated with the revision quality model."""

    def __init__(self, revision: Revision) -> None:
        max_vals = constants.MAX_FEATURE_VALS.get(revision.lang)
        if max_vals is None:
            raise UnsupportedWikiException(
                wiki=revision.lang, feature_group=self.__class__.__name__
            )

        self._revision = revision
        self._sqrt_len = math.sqrt(len(self._revision.text)) or 1
        self._max_vals = max_vals
        self._norm_length: Optional[float] = None
        self._norm_categories: Optional[float] = None
        self._norm_headings: Optional[float] = None
        self._norm_media: Optional[float] = None
        self._norm_references: Optional[float] = None
        self._norm_wikilinks: Optional[float] = None

    @property
    def norm_length(self) -> float:
        if self._norm_length is None:
            max_length = self._max_vals["length"]
            self._norm_length = min(self._sqrt_len, max_length) / max_length

        return self._norm_length

    @property
    def norm_categories(self) -> float:
        if self._norm_categories is None:
            max_categories = self._max_vals["categories"]
            self._norm_categories = (
                min(len(self._revision.categories), max_categories) / max_categories
            )

        return self._norm_categories

    @property
    def norm_headings(self) -> float:
        if self._norm_headings is None:
            max_headings = self._max_vals["headings"]
            headings = len(self._revision.headings) / self._sqrt_len
            self._norm_headings = min(headings, max_headings) / max_headings

        return self._norm_headings

    @property
    def norm_references(self) -> float:
        if self._norm_references is None:
            max_refs = self._max_vals["references"]
            refs = len(self._revision.references) / self._sqrt_len
            self._norm_references = min(refs, max_refs) / max_refs

        return self._norm_references

    @property
    def norm_media(self) -> float:
        if self._norm_media is None:
            max_media = self._max_vals["media"]
            self._norm_media = min(len(self._revision.media), max_media) / max_media

        return self._norm_media

    @property
    def norm_wikilinks(self) -> float:
        if self._norm_wikilinks is None:
            max_links = self._max_vals["wikilinks"]
            normalized_links = math.sqrt(len(self._revision.wikilinks)) / self._sqrt_len
            self._norm_wikilinks = min(normalized_links, max_links) / max_links

        return self._norm_wikilinks

    @property
    def quality_score(self) -> float:
        # These co-efficients have been empirically
        # determined and do not change depending on
        # the revision or language.
        coef_length = 0.395
        coef_media = 0.114
        coef_reference = 0.181
        coef_headings = 0.123
        coef_links = 0.115
        coef_categories = 0.070

        return (
            coef_length * self.norm_length
            + coef_media * self.norm_media
            + coef_reference * self.norm_references
            + coef_headings * self.norm_headings
            + coef_categories * self.norm_categories
            + coef_links * self.norm_wikilinks
        )

    @property
    def quality_range(self) -> float:
        return math.floor(10 * self.quality_score)

    def to_dict(self) -> Dict[str, Any]:
        return super().to_dict()


class FeatureNotFoundException(Exception):
    def __init__(self, feature_name: str) -> None:
        super().__init__(f"Feature {feature_name} not found in given sources")


@dataclass
class FeatureSource:
    # The Features object to be used for
    # retrieving features.
    source: Features
    # The prefix, if any, to be used with
    # features retrieved from source. This
    # helps when you want the same set of
    # features for the current and the parent
    # revision but want to differentiate between
    # the them.
    prefix: str = ""


Transformer = Callable[[Dict[str, Any]], Dict[str, Any]]


def get_features(
    feature_sources: Sequence[FeatureSource],
    features: Sequence[str],
    transformer: Optional[Transformer] = None,
) -> Dict[str, Any]:
    """Retrieve the requested `features` from
    `feature_sources` and optionally apply the
    transformations defined in `transformer` before
    returning. Throws `FeatureNotFoundException`
    if the final features don't contain the requested
    features or if transformer throws a `KeyError`.

    If more than one set of the same features is required,
    such as `ContentFeatures` for both current and parent
    revisions, give one or both Feature objects different
    prefixes while creating the FeatureSource objects to
    differentiate between their features.

    If a transformer is provided, it is passed a dictionary
    containing all the features from sources in `feature_sources`
    and the transformed features get added to the rest of the
    features before the final filter.
    """

    all_features = {
        fs.prefix + key: val
        for fs in feature_sources
        for key, val in fs.source.to_dict().items()
    }
    if transformer is not None:
        all_features.update(transformer(all_features))

    try:
        return {f: all_features[f] for f in features}
    except KeyError as e:
        raise FeatureNotFoundException(feature_name=e.args[0]) from e
