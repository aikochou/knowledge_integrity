# Knowledge Integrity

## About
`knowledge-integrity` is a suite of features developed around mediawiki revisions along with a set of tools that can help extend and modify these features for rapid development of new machine learning models.


## Installation
To install, run:
```bash
pip install git+https://gitlab.wikimedia.org/repos/research/knowledge_integrity.git@v0.4.0
```

## Example
Here's a simple example that shows how to get content features for both a revision and its parent revision:
```python
import asyncio
import aiohttp
import mwapi

from knowledge_integrity.featureset import ContentFeatures, FeatureSource, get_features
from knowledge_integrity.revision import get_current_revision


async def main() -> None:
    async with aiohttp.ClientSession() as s:
        session = mwapi.AsyncSession(
            "https://en.wikipedia.org",
            session=s,
        )
        revision = await get_current_revision(session, 3423342, "en")

        required_features = [
            "reference_count",
            "parent_reference_count",
            "media_count",
            "parent_media_count",
        ]

        content_features = ContentFeatures(revision)
        parent_content_features = ContentFeatures(revision.parent)

        # Wrapping the feature groups in FeatureSource helps us pass a
        # a prefix for all parent features
        feature_sources = (
            FeatureSource(source=content_features),
            FeatureSource(source=parent_content_features, prefix="parent_"),
        )

        print(get_features(feature_sources, required_features))


asyncio.run(main())

```

## Pre-trained Models
`knowledge-integrity` also comes with a number of pre-trained models developed by the research team at WMF. These models were built using the tools and utilities that `knowledge-integrity` provides and thus also serve as good examples of how this package can aid and expedite the development of models that target revisions.
* Serialized versions of these models can be downloaded from here: https://analytics.wikimedia.org/published/wmf-ml-models/revertrisk
* Functions for loading each model and using it to classify revisions can be found in the relevant module in the [models](https://gitlab.wikimedia.org/repos/research/knowledge_integrity/-/tree/main/knowledge_integrity/models) subpackage.

## Development

### Prerequisites
* Install the latest version of [Poetry](https://gitlab.wikimedia.org/repos/research/knowledge_integrity.git)

### Setup
After installing the prerequisites, run:
```bash
git clone https://gitlab.wikimedia.org/repos/research/knowledge_integrity.git
cd knowledge_integrity
poetry install
```

### Testing
To run all tests:
```bash
poetry run pytest -vv
```

### Hooks
To set up the pre-commit hooks, run:
```bash
poetry run pre-commit install
```
This will lint and typecheck the code on every commit. Hooks and tests are also run in the CI.

## Bugs and feature requests
You can report bugs and open feature requests using the issue tracker.

When reporting bugs, please provide a short, self-contained and correct example.

When opening a feature request, please also describe the problem you intend to solve with the feature.
